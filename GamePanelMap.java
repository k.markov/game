
import java.awt.Graphics;
import java.awt.Panel;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class GamePanelMap extends Panel
{
	GameFrame frame;
	GameKeyListener gkl;
	String [][] n;
	int br = 0;
	GamePanelMap(GameFrame frame)
	{
		this.frame = frame;
		gkl = new GameKeyListener(frame);
		addKeyListener(gkl);
		GameMouseListener gml = new GameMouseListener(this);
		addMouseListener(gml);
	}

	public void paint(Graphics g)
	{		
		br++;
		if(br < 11) n = frame.map()[0];
		else  n = frame.map()[1];

		BufferedImage img = null;
		BufferedImage spider = null;
		BufferedImage wizard = null;
		BufferedImage rock = null;
		BufferedImage chest = null;
		BufferedImage spiderSmallDead = null;
		try 
		{
			img = ImageIO.read(new File("dwarf.jpg"));
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		try 
		{
			spider = ImageIO.read(new File("spider.jpg"));
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		try 
		{
			wizard = ImageIO.read(new File("wizard.jpg"));
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		try 
		{
			rock = ImageIO.read(new File("rock.jpg"));
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		try 
		{
			chest = ImageIO.read(new File("chest.jpg"));
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		try 
		{
			spiderSmallDead = ImageIO.read(new File("spiderSmallDead.jpg"));
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		for (int i = 0; i < frame.map.getSizeY(); i++)
		{
			for (int j = 0; j < frame.map.getSizeX(); j++)
			{
				
				if (i == frame.getDwarf().getPosY() && j == frame.getDwarf().getPosX()) 
				{
					//n[i][j] = "X";
					g.drawImage(img, j * 75, i * 75, null);
				}
				else if (n[i][j].equals("s")) g.drawImage(spider,j * 75, i * 75, null);
				else if (n[i][j].equals("w")) g.drawImage(wizard, j * 75, i * 75, null);
				else if (n[i][j].equals("*")) g.drawImage(rock, j * 75 + 3, i * 75 + 3, null);
				else if (n[i][j].equals("c")) g.drawImage(chest, j * 75, i * 75, null);
				else if (n[i][j].equals("0")) g.drawRect(j * 75, i * 75, 75, 75);
				else if (n[i][j].equals("x")) g.drawImage(spiderSmallDead, j * 75 + 3, i * 75 + 3, null);

			}
		}
//		for (int i = 0; i < n.length; i++)
//		{
//			for (int j = 0; j < n[i].length; j++)
//			{
//				System.out.print(n[i][j] + " ");
//			}
//			System.out.println();
//		}
//		System.out.println(br);
	}
}
