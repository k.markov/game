import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Game
{
    public static void main(String args[])
    {
    	Map map = Dwarf.m;    	
    	GameFrame f = new GameFrame(map);
    	Dwarf Gimli = new Dwarf (100, 100, 1, 0, 0, f);    	    	

    	f.setSize(f.getMaximumSize());    	
    	f.setVisible(true);
    	f.setDwarf(Gimli);
    }
}

