
import java.awt.Graphics;
import java.awt.Panel;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class GamePanelBeginning extends Panel
{
	GameFrame frame;
	GameKeyListener gkl;
	GamePanelBeginning(GameFrame frame)
	{
		this.frame = frame;
		gkl = new GameKeyListener(frame);
		addKeyListener(gkl);
	}
	
	public void paint(Graphics g)
	{
		String [][] n = frame.beginning();
		BufferedImage img = null;
		try 
		{
			img = ImageIO.read(new File("D:\\From C\\Files\\school\\Informatics\\game\\Dwarfomania\\game-main\\dwarf.jpg"));
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		
		
		for (int i = 0; i < 3; i++)
		{
			for (int j = 0; j < frame.map.getSizeX(); j++)
			{
				
				if (i == frame.getDwarf().getPosY() && j == frame.getDwarf().getPosX()) 
				{
					n[i][j] = "X";
					g.drawImage(img, j * 75, i * 75, null);
				}
				else 
				{
					n[i][j] = "0";
					g.drawRect(j * 75, i * 75, 75, 75);
				}
			}
		}
	}
}
