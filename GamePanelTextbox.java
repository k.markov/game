
import java.awt.Color;
import java.awt.Font;
import java.awt.Label;
import java.awt.Panel;
import java.awt.TextArea;

public class GamePanelTextbox extends Panel
{
	TextArea textArea; 
	Label health;
	Label food;
	Dialogues d = new Dialogues();
	GamePanelTextbox()
	{
		
		textArea = new TextArea();
		textArea.setBounds(0, 0, 500, 250);
		textArea.setText("You are in the forest. There are spiders that are hostile and want to kill you. A wizard who has something for you stands at the end. Your tasks is to go to the wizard. \nYou can move with w, a, s, d. When you go to a spider you will be asked if you want to begin a battle.\nPress 'e' to eat an apple and gain 20 health\n\nIgrata e pulna s bugove koito ne moga da opravq");
		this.add(textArea);
		
		health = new Label();
		health.setBounds(0, 270, 150, 50);
		health.setBackground(Color.GRAY);
		health.setFont(new Font("Serif", Font.PLAIN, 25));
		this.add(health);
		
		food = new Label();
		food.setBounds(150, 270, 150, 50);
		food.setBackground(Color.GRAY);
		food.setFont(new Font("Serif", Font.PLAIN, 25));
		this.add(food);
	}
}
