
public class Dialogues 
{

	String story()
	{
		return "Welcome to Dwarfomania \nIn the world of Dwarfomania you are a dwarf who lives peacefully and carefree \nuntil one day your wife disappears without a trace in the dangerous mystical forest. \nThere is only one question before you. Are you ready to embark on an exciting\nadventure, facing the monsters and dangers of the mystical forest in search of \nthe truth about what happened to your wife? Well then let's get started!";
	}
	String instructions()
	{
		return "Instructions\nAs you enter the mystical forest, you will meet more and more of its inhabitants. \nSome of them are friendly and will help you, while others will try to stop you. \nIn the beginning you start with damage 100 and health 100. When you face a monster,\nyou will be able to see its stats. If you do more damage to his health, then \nyou will win the battle without any problems. Otherwise, it is better not to go into\nbattle, as you have only one life. If you die, the game is over. When you win \na battle, you gain experience and raise levels. With each level you become more resilient\nand stronger. Friendly creatures will help you during the adventure by giving you food\nto regain your health, stronger weapons and other bonuses. With each step, you will\nbe closer to the truth about your wife's disappearance.";
	}
	
	String talkToWizard()
	{
		return "Wizard: Hello, Stranger! What brings you in the mystical forest?\nYou: I have to save someone.\nWizard: And this someone is your wife, isn't it?\nYou: How do you know?\nWizard: Oh, my boy, I've been living here for 700 years now. I know everything that happens in the forest, everyone who enters the forest.\nYou: So you know where I can find her.\nWizard: I'm afraid I don't. I can tell you she is still alive. But you must hurry. I can give you this sword. With this you can easily kill the monsters of the forest. Take it.\nYou take the sword and look at its beauty. It gives you extra 50 damage. So you total damage is now 150.\nYou: Thank..uhh?\nThe wizard is gone. You look around but there is nobody. You hand the sword on your waist and continue your adventure deeper into the forest.\n";	
	}
	
	String talkToOldMan()
	{
		return "Old Man: Where do you think you are going, young man?\nYou: My wife was kidnapped. I have to save her.\nOld Man: It's too dangerous to go alone in the forest. Evil creatures live there. No one who enters comes back alive. I'm sorry but I believe your wife is long gone. \nYou: Nothing will stop me to save my love. I believe she is somewhere there in the dark.\nOld Man: Then so take this apples. There are magical and can heal you if you get hurt.\nThe Old Man gives you 3 golden apples. Each one of them gives you +20 health. You can eat them whenever you want. Just click 'e'.\nOld Man: Good luck my boy. Hope you find your loved one and come out the forest alive.\nYou: Thank you, Old Man!\n";
	}
	
}
