import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;

public class GameButtonListener implements ActionListener
{
	//GamePanelMain panel;
	GameFrame frame;
	Dialogues d;
	GameButtonListener(GameFrame frame)
	{
		this.frame = frame;
	}
	
	@Override
	public void actionPerformed(ActionEvent e)
	{
		if (e.getSource().equals(frame.panelMain.instButton)) frame.panelMain.textArea.setVisible(true);
		else if (e.getSource().equals(frame.panelMain.startButton)) 
		{
			frame.panelMain.setVisible(false);
			frame.getDwarf().talkToOldMan();	

		}
		else if (e.getSource().equals(frame.panelAction.button) && frame.panelAction.button.getText().equals("Enter the Forest")) 
		{
			frame.panelAction.button.setVisible(false);
			frame.panelAction.setVisible(false);
		}
		else if (e.getSource().equals(frame.panelAction.button) && frame.panelAction.button.getText().equals("End of Level 1"))
		{
			frame.panelAction.button.setVisible(false);
			frame.panelAction.setVisible(false);
			frame.panelEnd.setVisible(true);
    		frame.panelEnd.label.setText("End of Level 1");
		}
	}
}

































