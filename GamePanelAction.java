import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.ImageIcon;
import java.awt.Font;
import javax.swing.JButton;

public class GamePanelAction extends JPanel
{
	JTextArea textArea;
	JLabel enemy, dwarf, dwarfHealth, dwarfDamage, enemyHealth, enemyDamage;
	JButton button;
	GameKeyListener gkl;
	GameButtonListener gbl;
	GameFrame frame;

	public GamePanelAction(GameFrame frame) 
	{
		this.frame = frame;
		gkl = new GameKeyListener(frame);
		addKeyListener(gkl);
		
		gbl = new GameButtonListener(frame);
		
		setLayout(null);
		
		JLabel dwarf = new JLabel();
		dwarf.setIcon(new ImageIcon("dwarfBig.jpg"));
		dwarf.setBounds(70, 257, 350, 350);
		add(dwarf);
		
		textArea = new JTextArea();
		textArea.setBounds(124, 35, 808, 201);
		add(textArea);
		
		enemy = new JLabel();
		enemy.setBounds(647, 257, 350, 350);
		add(enemy);
		
		dwarfHealth = new JLabel();
		dwarfHealth.setFont(new Font("Arial Black", Font.PLAIN, 20));
		dwarfHealth.setBounds(430, 287, 207, 70);
		add(dwarfHealth);
		
		dwarfDamage = new JLabel();
		dwarfDamage.setFont(new Font("Arial Black", Font.PLAIN, 20));
		dwarfDamage.setBounds(430, 356, 207, 70);
		add(dwarfDamage);
		
		enemyHealth = new JLabel();
		enemyHealth.setFont(new Font("Arial Black", Font.PLAIN, 20));
		enemyHealth.setBounds(1007, 287, 207, 70);
		add(enemyHealth);
		
		enemyDamage = new JLabel();
		enemyDamage.setFont(new Font("Arial Black", Font.PLAIN, 20));
		enemyDamage.setBounds(1007, 356, 207, 70);
		add(enemyDamage);
		
		button = new JButton("Enter the Forest");
		button.setFont(new Font("Tahoma", Font.PLAIN, 30));
		button.setBounds(974, 36, 260, 189);
		add(button);
		
		button.addActionListener(gbl);
	}
}
