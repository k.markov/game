
import java.awt.Graphics;
import java.awt.Panel;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;

public class GamePanelEnd extends JPanel
{
	JLabel label;
	public GamePanelEnd() {
		setLayout(null);
		
		label = new JLabel("YOU DIED");
		label.setBounds(165, 316, 767, 90);
		label.setForeground(Color.RED);
		label.setFont(new Font("Informal Roman", Font.PLAIN, 74));
		add(label);
	}
	
}
