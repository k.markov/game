
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.ImageIcon;
import java.awt.Color;

public class GamePanelMain extends JPanel
{
	GameButtonListener gbl;
	GameFrame frame;
	JButton instButton, startButton;
	JTextArea textArea;
	Dialogues d = new Dialogues();
	private JTextArea textArea1;
	private final JLabel lblNewLabel = new JLabel("");
	private JLabel lblNewLabel_1;
	
	public GamePanelMain(GameFrame frame) 
	{
		this.frame = frame;
		gbl = new GameButtonListener(frame);
		setLayout(null);
		
		startButton = new JButton("Start");
		startButton.setFont(new Font("Tahoma", Font.PLAIN, 34));
		startButton.setBounds(335, 270, 221, 67);
		add(startButton);
		
		JLabel title = new JLabel("Dwarfomania");
		title.setForeground(Color.WHITE);
		title.setBackground(Color.ORANGE);
		title.setBounds(380, 66, 629, 91);
		title.setFont(new Font("Algerian", Font.PLAIN, 80));
		add(title);
		
		instButton = new JButton("Instructions");
		instButton.setFont(new Font("Tahoma", Font.PLAIN, 34));
		instButton.setBounds(835, 270, 221, 67);
		add(instButton);
		
		textArea = new JTextArea();
		textArea.setFont(new Font("Monospaced", Font.PLAIN, 13));
		textArea.setBounds(335, 348, 721, 202);
		textArea.setText(d.instructions());
		add(textArea);	
		textArea.setVisible(false);
		
		textArea1 = new JTextArea();
		textArea1.setFont(new Font("Monospaced", Font.PLAIN, 13));
		textArea1.setBounds(335, 168, 721, 91);
		add(textArea1);
		textArea1.setText(d.story());
		
		lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setIcon(new ImageIcon("forest.jpg"));
		lblNewLabel_1.setBounds(0, -608, 1990, 2071);
		add(lblNewLabel_1);
		
		instButton.addActionListener(gbl);
		startButton.addActionListener(gbl);
		
	}
}
