import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class GameKeyListener implements KeyListener 
{
	GameFrame frame;
	char c;
	GameKeyListener(GameFrame frame)
	{
		this.frame = frame;
	}
	
	@Override
	public void keyTyped(KeyEvent e) 
	{
		c = e.getKeyChar();
		frame.getDwarf().move(c);
		frame.panelMap.repaint();
	}

	@Override
	public void keyPressed(KeyEvent e) 
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyReleased(KeyEvent e) 
	{
		// TODO Auto-generated method stub
		
	}
}
