public class Map 
{
	private int sizeX, sizeY;

	Map(int sizeX, int sizeY)
	{
		setSizeX(sizeX);
		setSizeY(sizeY);
	}
	
	public int getSizeY() 
	{
		return sizeY;
	}
	
	public int getSizeX() 
	{
		return sizeX;
	}

	public void setSizeY(int sizeY) 
	{
		if (sizeY > 0 && sizeY <= 20) this.sizeY = sizeY;
		else System.out.println("The map can be up to 20 squares");
	}

	public void setSizeX(int sizeX) 
	{
		if (sizeX > 0 && sizeX <= 20) this.sizeX = sizeX;
		else System.out.println("The map can be up to 20 squares");
	}
}
