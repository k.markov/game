import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import javax.swing.ImageIcon;

public class Dwarf
{
    //fields---------------------------------------------------------------
    private int health, damage; 
    double experience = 0;
    private int level;
    private int posX, posY;
    static Map m = new Map(11, 11);
    Scanner scan = new Scanner(System.in);
    GameFrame frame;
    int apples = 3;
    Dialogues d = new Dialogues();
    String[][] a;
    //constructor----------------------------------------------------------
    Dwarf(int health, int damage, int level, int posX, int posY, GameFrame frame)
    {
        setHealth(health);
        setDamage(damage);
        setLevel(level);
        setPosX(posX);
        setPosY(posY);
        this.frame = frame;
        frame.panelTextbox.health.setText("Health: " + this.getHealth());
        frame.panelTextbox.food.setText("Apples:" + apples);
    }

    //getters-------------------------------------------------------------
    int getHealth()
    {
        return health;
    }
    
    int getDamage()
    {
        return damage;
    }
    
    double getExperience()
    {
    	return experience;
    }
    
    int getLevel()
    {
        return level;
    }
    
	public int getPosY() 
	{
		return posY;
	}
	
	public int getPosX() 
	{
		return posX;
	}
 
    //setters-------------------------------------------------------------
    void setHealth(int health)
    {
        if (health > 0) this.health = health;
        else System.out.println("Health must be greater than 0");
    }
    
    void setDamage(int damage)
    {
        if (damage >= 0) this.damage = damage;
        else System.out.println("Damage must be equal or greater than 0");
    }
    
    void setLevel(int level)
    {
        if (level >= 0) this.level = level;
        else System.out.println("Level must be equal or greater than 0");
    }
    
	public void setPosY(int posY) 
	{
		if (posY >= 0 && posY <= m.getSizeY()) this.posY = posY;
		else System.out.println("Not in the map");
	}

	public void setPosX(int posX) 
	{
		if (posX >= 0 && posX <= m.getSizeX()) this.posX = posX;
		else System.out.println("Not in the map");
	}
	
    //methods------------------------------------------------------------
    double eat(double food)
    {
        health += food;
        return health;
    }

    void defend(double enemyDamage)
    {
        health -= enemyDamage;
        //if (health <= 0)  System.out.println("You died!");
    }

    void attack (double enemyHealth)
    {
        enemyHealth -= damage;
        if (enemyHealth <= 0)
        {
            
            experience += 1000;
        }
    }
    boolean isWinner = false;
    
    void battle()
    {
    	frame.panelAction.enemy.setIcon(new ImageIcon("spiderBig.jpg"));
    	frame.panelAction.setVisible(true);
		frame.panelAction.setFocusable(true);
		frame.panelAction.setRequestFocusEnabled(true);
		frame.panelAction.grabFocus();
    	frame.panelAction.textArea.setText("You've encountered a spider. Its health is 20 and its damage is 20. \nDo you want to battle it? Press 'y' to fight.");
		frame.panelAction.enemyHealth.setText("Health: 20");
		frame.panelAction.enemyDamage.setText("Damage: 20");
		frame.panelAction.dwarfHealth.setText("Health: " + this.getHealth());
		frame.panelAction.dwarfDamage.setText("Damage: " + this.getDamage());
		if (frame.panelAction.gkl.c == 'y')
		{
			frame.panelAction.textArea.setText("The spider attacks first");
	        frame.getDwarf().defend(20);
	        if (this.getHealth() > 0) 
	        {
	        	 frame.panelAction.dwarfHealth.setText("Health: " + this.getHealth());
		        frame.panelAction.textArea.setText("Now you attack.\nPress Enter to exit");
		    
		        frame.getDwarf().attack(20);
		        frame.panelAction.enemyHealth.setText("Health: 0"); 
		        frame.panelAction.enemy.setIcon(new ImageIcon("spiderDead.jpg"));
		        
		        frame.panelTextbox.textArea.setText("Ne moga da gi nakaram da izchezvat");
				frame.getDwarf().experience += 100;
				isWinner = true;
		        frame.panelTextbox.health.setText("Health: " + this.getHealth());
				//frame.repaint();    		
		        //frame.panelAction.setVisible(false);
	        }
	        else frame.panelEnd.setVisible(true);

		}
		
		

//		else if (frame.panelMap.gkl.c == 'n')
//		{
//			frame.getDwarf().posX--;
//			frame.repaint();
//			frame.panelTextbox.textArea.setText("You've ran away from battle");
//		}
    }
    void talkToOldMan()
    {
    	frame.panelAction.setVisible(true);	
		frame.panelAction.setFocusable(true);
		frame.panelAction.setRequestFocusEnabled(true);
		frame.panelAction.grabFocus();
		frame.panelAction.enemy.setIcon(new ImageIcon("oldman.jpg"));
		frame.panelAction.textArea.setText("You are standing before the mystical forest. There's an old man at its entrance.\n" + d.talkToOldMan());
    }
    
    void talkToTheWizard()
    {
    	frame.panelAction.enemy.setIcon(new ImageIcon("wizardBig.jpg"));
    	frame.panelAction.setVisible(true);
		frame.panelAction.setFocusable(true);
		frame.panelAction.setRequestFocusEnabled(true);
		frame.panelAction.grabFocus();
    	frame.panelAction.textArea.setText("You've reached the wizard. Press 't' to talk to him.");
    	if (frame.panelAction.gkl.c == 't') 
		{
    		frame.panelAction.textArea.setText(d.talkToWizard());
    		frame.panelAction.button.setVisible(true);
    		frame.panelAction.button.setText("End of Level 1");

		}
    }
    
    boolean getIsWinner()
    {
    	return isWinner;
    }
    
    void moveUp()
    {
        a = frame.create();
    	if (posY == 0) frame.panelTextbox.textArea.setText("You can't go outside the map");
    	else if (a[getPosY() - 1][getPosX()].equals("*")) frame.panelTextbox.textArea.setText("You can't go through the stones");
    	else posY--;
    }
    
    void moveDown()
    {
    	a = frame.create();
    	if (posY + 2 > m.getSizeY()) frame.panelTextbox.textArea.setText("You can't go outside the map");
    	else if (a[getPosY() + 1][getPosX()].equals("*")) frame.panelTextbox.textArea.setText("You can't go through the stones");
    	else posY++;
    }
    
    void moveLeft()
    {
    	a = frame.create();
    	if (posX == 0) frame.panelTextbox.textArea.setText("You can't go outside the map");
    	else if (a[getPosY()][getPosX() - 1].equals("*")) frame.panelTextbox.textArea.setText("You can't go through the stones");
    	else posX--;
    }
    
    void moveRight()
    {
    	a = frame.create();
    	if (posX + 2 > m.getSizeX()) frame.panelTextbox.textArea.setText("You can't go outside the map");
    	else if (a[getPosY()][getPosX() + 1].equals("*")) frame.panelTextbox.textArea.setText("You can't go through the stones");
    	else posX++;
    }
    
    void move(char c)
    {
		switch(c)
		{
    		case 'w' : moveUp(); break;
    		case 's' : moveDown(); break;
    		case 'a' : moveLeft(); break;
    		case 'd' : moveRight(); break;
    		case 'e' : if(apples > 0) {eat(20); apples--; frame.panelTextbox.health.setText("Health: " + this.getHealth()); frame.panelTextbox.food.setText("Apples:" + apples);} else frame.panelTextbox.textArea.setText("No more apples"); break;
    		default: System.out.println();
		}
    }
}
