import java.awt.Button;
import java.awt.Color;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Label;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import javax.imageio.ImageIO;
import javax.swing.JFrame;

public class GameFrame extends Frame
{

	Map map;
	GameWindowListener gwl = new GameWindowListener();		

	GamePanelMain panelMain;
	GamePanelMap panelMap;
	GamePanelEnd panelEnd;
	GamePanelTextbox panelTextbox;
	GamePanelAction panelAction;
	String [][] a;
	BufferedImage bg = null;
	
	GameFrame(Map map)
	{
		this.map = map;
		this.addWindowListener(gwl);
		this.setLayout(null);
				
		panelMain = new GamePanelMain(this);
		panelMain.setSize(this.getMaximumSize());
		this.add(panelMain);
		
		panelEnd = new GamePanelEnd();
		panelEnd.setSize(this.getMaximumSize());
		panelEnd.setBounds(0, 0, 1901, 1901);
		panelEnd.setVisible(false);
		this.add(panelEnd);
		
		
		panelAction= new GamePanelAction(this);
		panelAction.setSize(this.getMaximumSize());
		this.add(panelAction);
		panelAction.setVisible(false);
		
		panelMap = new GamePanelMap(this);
		panelMap.setBounds(100, 100, 901, 901);
		panelMap.setLayout(null);
		this.add(panelMap);		
		
		panelTextbox = new GamePanelTextbox();
		panelTextbox.setBounds(1001, 100, 901, 901);
		panelTextbox.setLayout(null);
		this.add(panelTextbox);

//		panelMap.setFocusable(true);
//		panelMap.setRequestFocusEnabled(true);
//		panelMap.grabFocus();
		
	}
	
	
	private Dwarf dwarf;
	public void setDwarf(Dwarf dwarf)
	{
		this.dwarf = dwarf;
	}
	
	public Dwarf getDwarf()
	{
		return dwarf;
	}
	
	public String[][] create()
	{
		a = new String [map.getSizeX()][map.getSizeY()];
		for (int i = 0; i < a.length; i++)
		{
			for (int j = 0; j < a[i].length; j++)
			{
				a[i][j] = "0";
			}
		}
		a[5][10] = "w";
		a[1][2] = "s";
		a[3][0] = "s";
		a[6][1] = "s";
		a[4][2] = "s";
		a[1][5] = "s";
		a[3][6] = "s";
		a[7][6] = "s";
		a[0][9] = "s";
		a[9][2] = "s";
		//a[5][9] = "s";
		a[4][10] = "s";
		a[6][10] = "s";
		
		a[1][0] = "*";
		a[1][1] = "*";
		a[0][3] = "*";
		a[1][3] = "*";
		a[1][6] = "*";
		a[1][7] = "*";
		a[3][3] = "*";
		a[3][4] = "*";
		a[4][4] = "*";
		a[4][3] = "*";
		a[5][6] = "*";
		a[5][7] = "*";
		a[8][0] = "*";
		a[8][1] = "*";
		a[8][2] = "*";
		a[9][0] = "*";
		a[10][0] = "*";
		a[10][1] = "*";
		a[10][2] = "*";
		a[1][9] = "*";
		a[1][10] = "*";
		a[4][9] = "*";
		a[6][9] = "*";
		
		a[9][1] = "c";
		a[0][10] = "c";
		return a;

	}
	String [][] b1, b2;
	//String[][] b3 = this.create();
	public String[][][]  map()
	{
		String [][] b = this.create();

	
		
		//String [][] b2 = new String [map.getSizeX()][map.getSizeY()];
		
		if (a[dwarf.getPosY()][dwarf.getPosX()].equals("s"))
		{
			dwarf.battle();
			if (panelAction.gkl.c == '\n')
			{
				if (dwarf.getIsWinner()) 
				{
					panelAction.setVisible(false);
					
					a[dwarf.getPosY()][dwarf.getPosX()] = "x";
					dwarf.setPosX(1);
					//b[9][2] = "0";
					
					dwarf.isWinner = false;
					b1 = a;
					//b2 = a;
					//dwarf.frame.repaint();
				}
			}
		}
		//else if ((b[dwarf.getPosY()][dwarf.getPosX()].equals("0") || b[dwarf.getPosY()][dwarf.getPosX()].equals("x") ||  b[dwarf.getPosY()][dwarf.getPosX()].equals("*")) && b1 != this.create()) b1 = b2;
		else if (b[dwarf.getPosY()][dwarf.getPosX()].equals("w"))
		{
			dwarf.talkToTheWizard();
		}
		else if (a[dwarf.getPosY()][dwarf.getPosX()].equals("x")) System.out.println("gey");
		String [][][] x = {this.create(), b1};
		return x;
	}
	
}
